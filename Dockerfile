FROM python:3.8-alpine

RUN apk add --no-cache \
      bash \
      jq \
      perl

RUN apk add --no-cache --virtual .build-deps  \
      gcc \
      libc-dev && \
    pip install --upgrade pip && \
    pip install --no-cache-dir \
      awscli \
      aws-sam-cli \
      pytest \
      pytest-mock \
      boto3 \
      six==1.11.0 && \
    apk del --no-cache .build-deps

RUN apk add gcc libc-dev libffi-dev openssl-dev
RUN apk add --update nodejs npm
      
RUN npm install -g serverless \
      && \
      npm install --save-dev serverless-step-functions \
        serverless-pseudo-parameters \
        serverless-iam-roles-per-function

ENTRYPOINT [ "/bin/sh" ]

